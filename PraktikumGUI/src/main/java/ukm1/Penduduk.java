/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ukm1;

/**
 *
 * @author acer
 */
public abstract class Penduduk {
    private String nama;
    private String tempatTanggalLahir;
    public Penduduk(){
    }
    public Penduduk(String dataNama, String dataTempatTanggalLahir){
        nama = dataNama;
        tempatTanggalLahir = dataTempatTanggalLahir;
    }
    public void setNama(String dataNama){
        nama = dataNama;
    }
    public String getNama(){
        return nama;
    }
    public void setTempatTanggalLahir(String dataTempatTanggalLahir){
        tempatTanggalLahir = dataTempatTanggalLahir ;
    }
    public String getTempatTanggalLahir(){
        return tempatTanggalLahir;
    }
   public abstract double hitungIuran();
}
