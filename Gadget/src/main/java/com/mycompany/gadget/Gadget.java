/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gadget;

/**
 *
 * @author acer
 */
public class Gadget {
    private String merk;
    private String warna;
    private String prosesor;
    private String ram;
    
    public String getMerk(){
        return merk;
    }
    public void setMerk(String merk){
        this.merk = merk;
    }
    public String getWarna(){
        return warna;
    }
    public void setWarna(String warna){
        this.warna = warna;
    }
    public String getProsesor(){
        return prosesor;
    }
    public void setProsesor(String prosesor){
        this.prosesor = prosesor;
    }
    public String getRam(){
        return ram;
    }
    public void setRam(String ram){
        this.ram = ram;
    }
}
