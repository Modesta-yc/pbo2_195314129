/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gadget;

/**
 *
 * @author acer
 */
public class Tablet extends Gadget{
    private String rom;
    private String konektivitas;
    
    public void setRom(String rom){
        this.rom = rom;
    }
    public String getRom(){
        return rom;
    }
    public void setKonektivitas(String konektivitas){
        this.konektivitas = konektivitas;
    }
    public String getKonektivitas(){
        return konektivitas;
    }
}
