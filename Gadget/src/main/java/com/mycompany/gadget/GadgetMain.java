/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gadget;

/**
 *
 * @author acer
 */
public class GadgetMain {
    public static void main(String[] args) {
        Mahasiswa mhs = new Mahasiswa();
        mhs.setNama("Modesta Yolanda Christi");
        mhs.setNim("195314129");
        
        Handphone hp = new Handphone();
        hp.setMerk("Asus");
        hp.setWarna("Putih");
        hp.setProsesor("Intel Atom Z3560");
        hp.setRam("2 GB");
        hp.setRom("16 GB");
        hp.setKonektivitas("GSM,HSPA,LTE");
        
        Tablet tab = new Tablet();
        tab.setMerk("Samsung");
        tab.setWarna("Hitam");
        tab.setProsesor("Marvell PXA1088");
        tab.setRam("1,5 GB");
        tab.setRom("8 GB");
        tab.setKonektivitas("Wifi");
        
        Laptop laptop = new Laptop();
        laptop.setMerk("Lenovo ThinkPad T400");
        laptop.setWarna("Hitam");
        laptop.setProsesor("Core 2 Duo 2.53 GHz");
        laptop.setRam("2 GB");
        laptop.setHardisk("160 GB");
        laptop.setJumlahUSB(3);
        
        System.out.println("NAMA`:  "+mhs.getNama());
        System.out.println("NIM :   "+mhs.getNim());
        System.out.println("");
        System.out.println("HANDPHONE                       TABLET                        LAPTOP");
        System.out.println("MERK : "+hp.getMerk()+"                     MERK : "+tab.getMerk()+"                MERK : "+laptop.getMerk());
        System.out.println("WARNA : "+hp.getWarna()+"                   WARNA : "+tab.getWarna()+"                 WARNA : "+laptop.getWarna());
        System.out.println("PROSESOR:"+hp.getProsesor()+"       PROSESOR: "+tab.getProsesor()+"     PROSESOR: "+laptop.getProsesor());
        System.out.println("RAM : "+hp.getRam()+"                      RAM : "+tab.getRam()+"                  RAM : "+laptop.getRam());
        System.out.println("ROM : "+hp.getRom()+"                     ROM : "+tab.getRom()+"                    HARDISK : "+laptop.getHardisk());
        System.out.println("KONEKTIVITAS : "+hp.getKonektivitas()+"     KONEKTIVITAS : "+tab.getKonektivitas()+"           Jumlah USB : "+laptop.getJumlahUSB()+" Buah");
        
    }
}
