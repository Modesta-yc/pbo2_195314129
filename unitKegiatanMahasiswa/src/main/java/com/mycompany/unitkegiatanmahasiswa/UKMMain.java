/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.unitkegiatanmahasiswa;

/**
 *
 * @author acer
 */
public class UKMMain {
    public static void main(String[] args) {
        UKM ukm = new UKM();
        ukm.setNama("Pecinta Alam");
        Mahasiswa ketua = new Mahasiswa("1833211", "Veronika", "Palembang, 24 Oktober 1999");
        ukm.setKetua(ketua);
        
        Mahasiswa sekre = new Mahasiswa("182281", "Riyan Abdi", "Palembang 11 November 2000");
        ukm.setSekretaris(sekre);
        
        System.out.println("Data Keanggotaan");
        System.out.println("Nama UKM : "+ukm.getNama());
        System.out.println("");
        System.out.println("DAFTAR ANNGOTA");
        
        Penduduk[] pen = new Penduduk[2];
        Mahasiswa[] mhs = new Mahasiswa[1];
        mhs[0] = new Mahasiswa("163000","Vinsensius"," Tegal, 4 Mei 1998");
        pen[0] = mhs[0];
        
        MasyarakatSekitar[] masyarakat = new MasyarakatSekitar[1];
        masyarakat[0] = new MasyarakatSekitar("211","Yohana","Badau, 22 April 1997");
        pen[1] = masyarakat[0];
        
        double jum = 0; 
        double jumMhs = 0 , jumMas = 0;
        for(int i=0; i < pen.length; i++){
            System.out.println((i+1)+". NAMA : "+pen[i].getNama());
            System.out.println("    TTL :   "+pen[i].getTempatTanggalLahir());
            
            if (pen[i] instanceof Mahasiswa){
                Mahasiswa mhs1 = (Mahasiswa) pen[i];
                
                System.out.println("    NIM :   "+mhs1.getNim());
                System.out.println("    Iuran  : "+mhs1.hitungIuran());
                jumMhs = mhs1.hitungIuran();
                System.out.println("");
            }
            else if (pen[i] instanceof MasyarakatSekitar){
                MasyarakatSekitar msy = (MasyarakatSekitar) pen[i];
                System.out.println("    Nomor:   "+msy.getNomor());
                System.out.println("    Iuran   :  "+msy.hitungIuran());
                jumMas = msy.hitungIuran();
                System.out.println("");
            }
            System.out.println("");
        }
        jum=jumMhs+jumMas;
        System.out.println("Total Iuran Anggota :   "+jum);
        
        
    }
}
