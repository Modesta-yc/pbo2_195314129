/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.unitkegiatanmahasiswa;

/**
 *
 * @author acer
 */
public class UKM {
    private String namaUnit;
    private Mahasiswa ketua;
    private Mahasiswa sekretaris;
    private Penduduk[] anggota;
    
    public UKM(){
    }
    public UKM(String namaUnit){
        this.namaUnit = namaUnit;
    }
    public void setNama(String namaUnit){
        this.namaUnit = namaUnit;
    }
    public String getNama(){
        return namaUnit;
    }
    public void setKetua(Mahasiswa dataKetua){
        ketua = dataKetua;
    }
    public Mahasiswa getKetua(){
        return ketua;
    }
    public void setSekretaris(Mahasiswa dataSekretaris){
        sekretaris = dataSekretaris;
    }
    public Mahasiswa getSekretaris(){
        return sekretaris;
    }
    public void setAnggota(Penduduk[] dataAnggota){
        anggota = dataAnggota;
    }
    public Penduduk[] getAnggota(){
        return anggota;
    }
}
